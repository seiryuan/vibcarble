/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.seiryuan.android.vcarcontrol;

import java.util.UUID;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for
 * incoming connections, a thread for connecting with a device, and a
 * thread for performing data transmissions when connected.
 */
class BluetoothComm {
    // Debugging
    private static final String TAG = "BLE-VibCar";

    // Member fields
    private Context mContext;
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private byte[] mBuffer;

    // Buffer for 1line (boundary is different from buffer)
    private final int LBUF_LENGTH = 501;
    private int[][] lbuf = new int[2][LBUF_LENGTH];
    private int lbuf_active = 0;
    private int lbuf_index = 0;

    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothGattCharacteristic mCharGPIOInput;
    private BluetoothGattCharacteristic mCharGPIOOutput;

    private int mConnectionState = STATE_DISCONNECTED;
    static final int STATE_DISCONNECTED = 0;
    static final int STATE_CONNECTING = 1;
    static final int STATE_CONNECTED = 2;
    static final int STATE_DISCONNECTING = 3;

    private final static UUID UUID_SERVICE = UUID.fromString(VibcarGattAttributes.VCAR_SERVICE);
    private final static UUID UUID_GPIO_OUTPUT = UUID.fromString(VibcarGattAttributes.CHARACTERISTIC_GPIO_OUTPUT);
    private final static UUID UUID_GPIO_INPUT = UUID.fromString(VibcarGattAttributes.CHARACTERISTIC_GPIO_INPUT);
    private final static UUID UUID_CONFIG = UUID.fromString(VibcarGattAttributes.CLIENT_CHARACTERISTIC_CONFIG);

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(TAG, "*****ConnectionStateChanged: status="+status+", newState="+newState);
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mConnectionState = STATE_CONNECTED;
                sendToastMsg("Connected");
                mHandler.obtainMessage(MainActivity.MESSAGE_STATE_CHANGE, mConnectionState, -1).sendToTarget();
                Log.d(TAG, "*****Connected to Device.");
                // Attempts to discover services after successful connection.
                if(mBluetoothGatt.discoverServices()) {
                    Log.d(TAG, "*****Start discover service.");
                }
                else {
                    Log.d(TAG, "*****Fail to discover service.");
                }

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d(TAG, "*****  Disconnected.");
                mConnectionState = STATE_DISCONNECTED;
                mHandler.obtainMessage(MainActivity.MESSAGE_STATE_CHANGE, mConnectionState, -1).sendToTarget();
            } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                Log.d(TAG, "*****  Connecting.");
                mConnectionState = STATE_CONNECTING;
                mHandler.obtainMessage(MainActivity.MESSAGE_STATE_CHANGE, mConnectionState, -1).sendToTarget();
            } else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                Log.d(TAG, "*****  Disconnecting.");
                mConnectionState = STATE_DISCONNECTING;
                mHandler.obtainMessage(MainActivity.MESSAGE_STATE_CHANGE, mConnectionState, -1).sendToTarget();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {

                BluetoothGattService mGattService = gatt.getService(UUID_SERVICE);
                if(mGattService == null) {
                    Log.d(TAG, "Service not found.");
                    return;
                }

                mCharGPIOOutput = mGattService.getCharacteristic(UUID_GPIO_OUTPUT);
                if(mCharGPIOOutput == null) {
                    Log.d(TAG, "Characteristic:GPIOOutput not found.");
                    return;
                }
                int prop1 = mCharGPIOOutput.getProperties();
                if((prop1 & BluetoothGattCharacteristic.PROPERTY_WRITE) == 0) {
                    Log.d(TAG, "Characteristic:GPIOOutput doesn't support WRITE.");
                    return;
                }

                mCharGPIOInput = mGattService.getCharacteristic(UUID_GPIO_INPUT);
                if(mCharGPIOInput == null) {
                    Log.d(TAG, "Characteristic:GPIOInput not found.");
                    return;
                }
                int prop2 = mCharGPIOInput.getProperties();
                if((prop2 & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0) {
                    gatt.setCharacteristicNotification(mCharGPIOInput, true);
                    BluetoothGattDescriptor descriptor = mCharGPIOInput.getDescriptor(UUID_CONFIG);
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    gatt.writeDescriptor(descriptor);
                }
            } else {
                Log.w(TAG, "Error in ServicesDiscover: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            Log.d(TAG, "*****Charasteristic["+characteristic.getUuid()+"] READ."+status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (UUID_GPIO_INPUT.equals(characteristic.getUuid())) {
                    final byte[] data = characteristic.getValue();
                    if (data != null && data.length > 0) {
                        for(int i = 0; i < data.length; i++) {
                            lbuf[lbuf_active][i] = data[i];
                        }
                        mHandler.obtainMessage(MainActivity.MESSAGE_READ, 0, -1).sendToTarget();
                        Log.d(TAG, "LEN:"+data.length+" DATA:"+data[0]+","+data[1]);
                    }
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            byte data[] = characteristic.getValue();
            if (data != null && data.length > 0) {
                for (int i = 0; i < data.length; i++) {
                    mBuffer[i] = data[i];
                }
            }
            Log.d(TAG, "Characteristic Update: ["+data[0]+"] ["+data[1]+"]");
            mHandler.obtainMessage(MainActivity.MESSAGE_READ, 0, -1).sendToTarget();
        }
    };

    /**
     * Constructor. Prepares a new BluetoothCommunication session.
     * @param context  A Reference to Main Activity to access SystemService
     * @param handler  A Handler to send messages back to the UI Activity
     */
    BluetoothComm(Context context, Handler handler, byte[] buffer) {
        mContext = context;
        mHandler = handler;
        mBuffer = buffer;
        BluetoothManager mManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        if (mManager != null) {
            mAdapter = mManager.getAdapter();
            if (mAdapter == null) {
                Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            }
        }
        else {
            Log.e(TAG, "Unable to initialize BluetoothManager.");
            mAdapter = null;
        }
        mConnectionState = STATE_DISCONNECTED;
    }

    /**
     * Return the current connection state. */
    synchronized int getState() {
        return mConnectionState;
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * @param out The bytes to write
     */
    void write(byte[] out) {
        Log.d(TAG, "Characteristic Write: ["+out[0]+"] ["+out[1]+"]");
        mCharGPIOOutput.setValue(out);
        mBluetoothGatt.writeCharacteristic(mCharGPIOOutput);
    }
    
    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void sendToastMsg(String str) {
        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(MainActivity.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.TOAST, str);
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }


    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {
        if (mAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (mBluetoothDeviceAddress != null && address.equals(mBluetoothDeviceAddress)
                && mBluetoothGatt != null) {
            Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                mHandler.obtainMessage(MainActivity.MESSAGE_STATE_CHANGE, mConnectionState, -1).sendToTarget();
                return true;
            } else {
                return false;
            }
        }

        Log.d(TAG, "***** start connecting to "+address);
        final BluetoothDevice device = mAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        mBluetoothGatt = device.connectGatt(mContext, true, mGattCallback);
        //mBluetoothGatt.connect();
        mBluetoothDeviceAddress = address;
        mConnectionState = STATE_CONNECTING;
        mHandler.obtainMessage(MainActivity.MESSAGE_STATE_CHANGE, mConnectionState, -1).sendToTarget();

        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (mAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }
}
