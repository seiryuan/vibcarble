package com.seiryuan.android.vcarcontrol;


import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

public class PreferenceActivity extends Activity {
    public static final String PREF_KEY_AUTO_CONNECT = "auto_connect";
    public static final String PREF_KEY_DEVICE_ADDR = "device_addr";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new CustomPreferenceFragment()).commit();
    }
    
    public static class CustomPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preference_screen_sample);
            
            SwitchPreference prefAutoConnect = (SwitchPreference)findPreference(PREF_KEY_AUTO_CONNECT);
            if(prefAutoConnect.isChecked()) {
            	prefAutoConnect.setSummary(prefAutoConnect.getSwitchTextOn());
            }
            else {
            	prefAutoConnect.setSummary(prefAutoConnect.getSwitchTextOff());                   	
            }

            EditTextPreference prefDeviceAddr = (EditTextPreference)findPreference(PREF_KEY_DEVICE_ADDR);
            prefDeviceAddr.setSummary(prefDeviceAddr.getText());
        }

        @Override
        public void onResume() {
            super.onResume();
            SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
            sharedPreferences.registerOnSharedPreferenceChangeListener(onPreferenceChangeListenter);
        }
        
        @Override
        public void onPause() {
            super.onPause();
            SharedPreferences sharedPreferences = getPreferenceScreen().getSharedPreferences();
            sharedPreferences.unregisterOnSharedPreferenceChangeListener(onPreferenceChangeListenter);
        }
        
        private OnSharedPreferenceChangeListener onPreferenceChangeListenter = new OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equals(PREF_KEY_AUTO_CONNECT)) {
                    SwitchPreference pref = (SwitchPreference)findPreference(key);
                    if(pref.isChecked()) {
                    	pref.setSummary(pref.getSwitchTextOn());
                    }
                    else {
                    	pref.setSummary(pref.getSwitchTextOff());                   	
                    }
                }
            }
        };
        
        /*
        private String multiSelectListSummary(String prefKey) {
            MultiSelectListPreference pref = (MultiSelectListPreference)findPreference(prefKey);
            ArrayList<CharSequence> summaryArray = new ArrayList<CharSequence>();
            CharSequence[] entries = pref.getEntries();
            CharSequence[] entryValues = pref.getEntryValues();
            Set<String> values = pref.getValues();
            for (CharSequence entryValue : entryValues) {
                if (values.contains(entryValue)) {
                    int index = pref.findIndexOfValue(entryValue.toString());
                    if (index >= 0) {
                        summaryArray.add(entries[index]);
                    }
                }
            }
            return TextUtils.join(", ", summaryArray);
        }
        */
    }    
}
