package com.seiryuan.android.vcarcontrol;


import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

//import android.os.PowerManager.WakeLock;

/**
 * This is the main Activity that displays the current chat session.
 */
public class MainActivity extends Activity {
    // Debugging
    private static final String TAG = "emCM main";
    private static final boolean D = false; // Set 'true' for debug message

    //
    //private View mView;
    private GraphView graph;
    private static ActionBar mActionBar;

    // bluetooth Connection Info
    public static boolean mDeviceConnected = false;
    public static String mConnectedDeviceName = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothComm mBluetoothComm = null;
    public byte[] mRxBuffer = new byte[20];

    // Message types sent from the BluetoothComm Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private static final int REQUEST_SETTING = 5;
    
    // emCM settings
    public static class Preference {
    	static boolean AutoConnect;
    	static String DeviceAddr;
    }
    public static boolean mDemoMode = false;

    // Screen brightness Control
    private PowerManager.WakeLock mWakeLock;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(D) Log.e(TAG, "+++ ON CREATE +++");

        // Viewの初期化
        graph = new GraphView(this);
        setContentView(graph);
        
        // アクションバーのステータス表示を初期化
        mActionBar = getActionBar();
    	if(mActionBar != null) mActionBar.setSubtitle(R.string.title_not_connected);
    	
    	// Preferenceを初期化（初回起動時のみ初期値を設定、後は保存値の復帰）
    	setPreference();
    	
        // PowerManager取得（アプリ実行中はスリープしないように設定するため）
    	PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
    	mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag");
    }

    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");

        // Bluetooth adapterが存在するかチェック
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
        }

        // BluetoothがOFFになっていたらON要求（→ダイアログで確認させる）
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // すでにONだったらBluetooth通信スレッドを初期化
        } else {
            if (mBluetoothComm == null) initDevice();

            // PreferenceでAuto Connectionになっている場合は直ちに接続開始
            if((Preference.AutoConnect)&&(Preference.DeviceAddr != null)) {
                try {
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(Preference.DeviceAddr);
                    connectDevice(device);
                } catch (IllegalArgumentException e) {
                    Log.d(TAG, "Invalid AutoConnect Address.");
                }
            }
        }

        // スリープ禁止
    	if(mWakeLock != null) mWakeLock.acquire();
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(TAG, "-- ON STOP --");
        disconnectDevice();

        // スリープ禁止を解除
        if(mWakeLock != null) mWakeLock.release();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(D) Log.e(TAG, "--- ON DESTROY ---");
    	mDemoMode = false;
   }

    // Handler process from BluetoothComm Thread
    private final Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothComm.STATE_CONNECTED:
                            mActionBar.setSubtitle(getString(R.string.title_connected_to, mConnectedDeviceName));
                            mDeviceConnected = true;
                            break;
                        case BluetoothComm.STATE_CONNECTING:
                            mActionBar.setSubtitle(R.string.title_connecting);
                            break;
                        case BluetoothComm.STATE_DISCONNECTED:
                            mActionBar.setSubtitle(R.string.title_not_connected);
                            mDeviceConnected = false;
                            break;
                    }
                    break;
                case MESSAGE_WRITE: // not use
                    break;
                case MESSAGE_READ:
                    graph.invalidate(); // screen redraw (cause onDraw() event)
                    graph.redraw();
                    break;
                case MESSAGE_DEVICE_NAME: // show the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), "Connected to "+mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                    break;
            }
            return true;
        }
    });

    //
    private void setPreference() {
        
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        Editor e;

        e = pref.edit();
        if (!pref.getBoolean("Launched", false)) {
        	Preference.AutoConnect = false;
        	e.putBoolean(PreferenceActivity.PREF_KEY_AUTO_CONNECT, Preference.AutoConnect);
        	e.putString(PreferenceActivity.PREF_KEY_DEVICE_ADDR, Preference.DeviceAddr);
        	e.putBoolean("Launched", true);  
        	e.apply();
        }
        else {  
        	updatePreference();
        }
    }

    //
    private void saveDeviceAddr(String addr) {
    	SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    	Editor e = sharedPreferences.edit();
    	e.putString(PreferenceActivity.PREF_KEY_DEVICE_ADDR, addr);
    	e.apply();
    }

    //
    private void updatePreference() {
    	SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
    	Preference.AutoConnect = pref.getBoolean(PreferenceActivity.PREF_KEY_AUTO_CONNECT, Preference.AutoConnect);
    	Preference.DeviceAddr = pref.getString(PreferenceActivity.PREF_KEY_DEVICE_ADDR, Preference.DeviceAddr);
    }

    //
    public void initDevice() {
        Log.d(TAG, "initDevice()");
        mBluetoothComm = new BluetoothComm(this, mHandler, mRxBuffer);
    }

    // connect request to bluetooth device
    private void connectDevice(BluetoothDevice device) {
        mBluetoothComm.connect(device.getAddress());
    }

    // disconnect from bluetooth device
    private void disconnectDevice() {
        if (mBluetoothComm != null) {
            mBluetoothComm.disconnect();
        }
        mDeviceConnected = false;
    }

    // デバイスにデータ送信
    public void sendMessage(String message) {
        byte data[] = new byte[2];

        //コネクションが確立されている事をチェック
        if (mBluetoothComm.getState() != BluetoothComm.STATE_CONNECTED) {
            return;
        }

        if (message.equals("L0\r")) {
            data[0] = 0x01;
            data[1] = 0x00;
        }
        else if (message.equals("L1\r")) {
            data[0] = 0x01;
            data[1] = 0x01;
        }
        mBluetoothComm.write(data);
    }

    //
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE_INSECURE:
            if (resultCode == Activity.RESULT_OK) {
                String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                saveDeviceAddr(address);
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                connectDevice(device);
            }
            break;
        case REQUEST_SETTING:
        	updatePreference();
            break;
        case REQUEST_ENABLE_BT:
            if (resultCode == Activity.RESULT_OK) {
                initDevice();
            } else {
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    // 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    // 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent serverIntent;
        switch (item.getItemId()) {
        case R.id.insecure_connect_scan:
            serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
            return true;
        case R.id.disconnect:
        	disconnectDevice();
        	return true;
        case R.id.setup_app:
            serverIntent = new Intent(this, PreferenceActivity.class);
            startActivityForResult(serverIntent, REQUEST_SETTING);
            return true;
        }
        return false;
    }
}
