package com.seiryuan.android.vcarcontrol;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class GraphView extends View{
	private Paint paint;
    private MainActivity mActivity;
    private boolean state_motor_left;
    private boolean state_motor_right;
    private boolean state_motor_forward;

	public GraphView(Context context){
		super(context);
 		mActivity = (MainActivity) context;
		paint = new Paint();

        state_motor_left = false;
        state_motor_right = false;
        state_motor_forward = false;
	}

	public void onDraw(Canvas canvas)	{
		int offset_x = 100, offset_y = 120;
		
 		super.onDraw(canvas);		
		canvas.drawColor(Color.BLACK); 			
		paint.setStrokeWidth(2);

        // Draw buttons
        draw_button(canvas, offset_x, offset_y+100, "LEFT", state_motor_left);
        draw_button(canvas, offset_x+200, offset_y+100, "FWD", state_motor_forward);
        draw_button(canvas, offset_x+400, offset_y+100, "RIGHT", state_motor_right);

        draw_button(canvas, offset_x, offset_y+300, "SW", (mActivity.mRxBuffer[1] == 1));

    }

    private void draw_button(Canvas canvas, float x,float y, String label, boolean state) {
        if(state)
            paint.setColor(Color.rgb(180, 220, 220));
        else
            paint.setColor(Color.rgb(120, 120, 120));

        canvas.drawRect(x, y, x+160, y+100, paint);
        paint.setAntiAlias(true);
        paint.setTextSize(40f);
        paint.setColor(Color.BLACK);
        canvas.drawText(label, x+16, y+40, paint);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x, y;
        int action;

        x = event.getX();
        y = event.getY();
        action = event.getAction();
        if(action == MotionEvent.ACTION_DOWN) {
            // Run/Stop
            if((x > 100)&&(x < 260)&&(y > 220)&&(y < 320)) {
                state_motor_left = true;
                mActivity.sendMessage("L1\r");
            }
            if((x > 300)&&(x < 460)&&(y > 220)&&(y < 320)) {
                state_motor_forward = true;
                mActivity.sendMessage("L1\r");
                mActivity.sendMessage("R1\r");
            }
            if((x > 500)&&(x < 660)&&(y > 220)&&(y < 320)) {
                state_motor_right = true;
                mActivity.sendMessage("R1\r");
            }
            redraw();
        }
        else if(action == MotionEvent.ACTION_UP) {
            // Run/Stop
            if((x > 100)&&(x < 260)&&(y > 220)&&(y < 320)) {
                state_motor_left = false;
                mActivity.sendMessage("L0\r");
            }
            if((x > 300)&&(x < 460)&&(y > 220)&&(y < 320)) {
                state_motor_forward = false;
                mActivity.sendMessage("L0\r");
                mActivity.sendMessage("R0\r");
            }
            if((x > 500)&&(x < 660)&&(y > 220)&&(y < 320)) {
                state_motor_right = false;
                mActivity.sendMessage("R0\r");
            }
            redraw();
        }
        return true;
    }

	public void redraw() {
		invalidate();
	}
}
